<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/21
 * Time: 15:56
 */
namespace chj\payments\Adapters;
use chj\payments\RequestHelper;

abstract class PaymentAbstract{

    public $serverHost;

    public function __construct()
    {
        $this->serverHost = RequestHelper::host();
    }

    /**
     * 页面支付
     * @param array $data
     */
     abstract  protected function payWebHandle(array $data);

    /**
     * 退款
     * @param array $data
     */
    abstract  protected function refundHandle(array $data);

    /**
     * app端支付
     * @param array $data
     */
     abstract  protected function payAppHandle(array $data);

    /**
     * 异步回调
     * @param array $data
     */
     abstract  protected function notifyHandle(array $data);

    /**
     * @param $payWay   支付方式
     * @return array
     */
    public function getPayInfo($payWay,$body=''){
        $return_data = [];
        if ($payWay){
            $return_data['body'] = "订单";
            $return_data['return_url'] = $this->serverHost.'/user/myPcOrders';
            $return_data['notify_url'] = $this->serverHost.'/api/orderPay/notify/'.$payWay;
        }
        return $return_data;
    }

}