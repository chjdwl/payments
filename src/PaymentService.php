<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/21
 * Time: 15:24
 */
namespace chj\payments;


/**
 * Class PayAdapter
 * @package App\Adapters
 */
class PaymentService
{
    /**
     * @var
     */
    protected $app;

    /**
     * 支付服务模块
     * @var array
     */
    protected $instance = [
        'wopay'=> 'chj\payments\WoPay\WoPaymentService',
        'alipay'=> 'chj\payments\Alipay\AlipaymentService'
    ];

    /**
     * 根据类型创建对应模块处理服务
     * PaymentAdapter constructor.
     * @param String $type
     */
    public function __construct($type,$config=[])
    {
        try{
            $instanceName = $this->instance[strtolower($type)];
        }catch (\Exception $exception) {
            throw new \Exception('不存在的支付类型');
        }

        if (class_exists($instanceName)) {
            $this->app = new $instanceName($config);
        }else{
            throw new \Exception('不存在的支付类型');
        }
        return $this;
    }


    /**
     * 处理web支付
     * @param array $params 支付提交的数据
     * @return bool
     */
    public function payWebHandle($params = [])
    {
        if (get_class($this->app)) {
            return $this->app->payWebHandle($params);
        }

        return false;
    }

    /**
     * 处理wap支付
     * @param array $params 支付提交的数据
     * @return bool
     */
    public function payWapHandle($params = [])
    {
        if (get_class($this->app)) {
            return $this->app->payWapHandle($params);
        }

        return false;
    }

    /**
     * 处理app支付
     * @param array $params 支付提交的数据
     * @return bool
     */
    public function payAppHandle($params = [])
    {
        if (get_class($this->app)) {
            return $this->app->payAppHandle($params);
        }

        return false;
    }

    /**
     * 处理支付回调
     * @param array $params 回调返回的数据
     * @return bool
     */
    public function notifyHandle($params = [])
    {
        if (get_class($this->app)) {
            return $this->app->notifyHandle($params);
        }

        return false;
    }

}