<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/22
 * Time: 13:40
 */
namespace chj\payments\Alipay;
use chj\payments\Adapters\PaymentAbstract;
use chj\payments\Alipay\Aop\AlipayTradePagePayContentBuilder;

class AlipaymentService extends PaymentAbstract{

    private $config;

    public function __construct($config=[])
    {
        $this->config = $config ;
    }

    public function payWebHandle(array $data)
    {
        // TODO: Implement payWebHandle() method.
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = trim($data['out_trade_no']);
        //订单名称，必填
        $subject = trim($data['subject']);
        //付款金额，必填
        $total_amount = trim($data['total_amount']);
        //商品描述，可空
        $body = trim($data['body']);
        //构造参数
        $payRequestBuilder = new AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
        $aop = new AlipayTradeService($this->config);
        $response = $aop->pagePay($payRequestBuilder,$this->config['return_url'],$this->config['notify_url']);

        //输出表单
        var_dump($response);exit;

    }

    public function payAppHandle(array $data)
    {
        // TODO: Implement payAppHandle() method.
        // TODO: Implement payWebHandle() method.
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = trim($data['out_trade_no']);
        //订单名称，必填
        $subject = trim($data['subject']);
        //付款金额，必填
        $total_amount = trim($data['total_amount']);
        //商品描述，可空
        $body = trim($data['body']);
        //构造参数
        $payRequestBuilder = new AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
        $aop = new AlipayTradeService($this->config);
        $response = $aop->appSign($payRequestBuilder,$this->config['notify_url']);
        return $response;
        //输出表单
        var_dump($response);exit;
    }

    public function refundHandle(array $data)
    {
        // TODO: Implement refundHandle() method.
    }

    public function notifyHandle(array $data)
    {
        // TODO: Implement notifyHandle() method.
    }


}