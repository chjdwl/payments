<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/21
 * Time: 15:36
 */
namespace chj\payments\Wopay;

use chj\payments\Adapters\PaymentAbstract;
use chj\payments\RequestHelper;

class WoPaymentService extends PaymentAbstract {

    protected $config;
    protected $payment;
    protected $pay_way = "wopay";

    public function __construct()
    {
        parent::__construct();

        $this->config = [
            'interfaceVersion'=> '2.0.0.0',
            'signType' => 'RSA_SHA256',
            'tranType'=>'20101',
            'charSet'=>'UTF-8',
            'tradeMode'=>'0001',
            'respMode'=>1,
            'RSA_PRIVATE_KEY'=> 'MIIJJwIBAAKCAgEAtqGkV0QMOQUP4aZa6h9+SG26kDurhjeY9yUB9MVJzuQhPxL7pICinYK/CQn3CfRKI6LoMigay0y1fehl07v2ff0EaM5S4l/HtWIuc09nUVqRykdpRgIXX13gXRFlbKdKR5XFMZDbYiqBlSxbEl1iLDl4SSD9Le/EYxXhNr50JTFBgL/+Yr01zc/2D/eBn7o+jU8LkUQE0306s5jtFCkIUm6NNazvbjDPJAlaNJr+mTq+YvkjCpqMXPUYM2CvdcutcY5RvK00SgSbs7vil6FoAEm8jvfJUVHTFURUzIrt/IsdmUUg+pOIvqFgCwjyhzRq9fk+BX09d0KPp0A5YPBGqCVIInn5pGulGKVLHCBIYIVI1vXDl/1BT85JWhWv+xzZOinM7UFgkhJmGxPV/tNfiFiOrCvgKhQZh07vR9z1+KgzcYTUHGXsW8Jgm2bvftQVRyoQtI2rRcBb/BObVLVOwsj+Za0Pbrai0u7NJaK5lo850U57B+yEKRNPlNRwVTOSVBm+PXDihzrdIkUzq8qS9Eogq/Ny67LfsRpO1UwuM+jKog+5MAbtQqhm4exfiYNhEV4w8HQB5fbuubHj56gVbPgljRrHB1pno7Y3dJ/+gCgzEaCIdg0d4i/5afKTOUM3G0Wb9Tgdr1d5tYkEfuFEiO2nn+Ton9l43inf/Qm/zvkCAwEAAQKCAgB7jbVxuD6QoMUSwomFFJNJirqJc2TiI2Df6nB9WK9LQXjghAYsTz40U0zMX6p6WIODwmVV8V3XJeku6fdhnCrxJyhBgzv9nBCuvGJybkiWeDBjaZZl32pSgV8Vd3xnWnHdJgzHMJnKurj5440lSnntM2AWETRXTHtPEXFQqN5Ifx8ybDE8OYdZ2pQ8TxSpq8U/sjdAKoUdUv5QnGffmLbhlZsAsiM1J94rIgydxDH0FhLRQH0dYlsNVF7v+zwY9Bc92KuheogCf0Wa/cQZEwAaGchv7GCNHE8ydwzraB4DGVaH54WdIAyF8wTX0lDJeYPXLqxv6D+aHDS1ULTAySzy1ckIueWnr4RghgZlghD80DOTiQxLQkBY63CfSZ170GqP+0xB9GAGsongFykQoS2RqokFjiPT1Wioa0uQe121p+TeCuhZkxUoobb0nnzHuHfXG7FLTk90HWdxMA5EuxUXlK455wSS989VVs9VXwzDDRi6qDHU/kxjy5WAg6xiErQZRK+DzapqaJoiUswL2Sps/fbvYa6rGZSHPYP+OckLCeFcYtk6It3dRC+H1P1dm3eN5cNeJ2rtp3mCvx+VCUQjfnr1q5dIqqDb5BoeXtxh9A7pATM7t//UDeandZKpNyfR2STi/bFoMbuj6ik/cTK7xhIhk09W01Gat+BciHFqNQKCAQEA3UpvkjpnO/xtLBO/YHmt26WxuB/4KoA12LjgEu1KR6KtY70m5xurLgKX3iPriR3S7uIkqQwLfSTFOdUFzUADtxPS93o8fGrw5fLVARUx4l6AOcjnlBHEtouezd9UpLXiMozwKcgVFZ0H9PHioYLl/h0k/BgOdX+Y/4ZYv73jA3jMhe28/LZOzNkFHVW1XjubC0WpC2jaEJptxGL57F4gVHwr2rKqzyCnbsexBxd+OnwwQ4N3pUf4vT39tHVv2IRYjFdXAH1UjYpX6AH8tl9k83G26P1x2+99cojMgzs8Vu3yxQAmRRrThjV1wIzL9+8ErIQCBXxqV0OdmusFw7M4twKCAQEA00bnZwmartGCQDpNzzEw1wG945tXufmI+211iNsy3+GVc+cPqTGJICVYsiWIWw1OGsHdFIbFjco74mVQpZQanQ05UBfHAa1HLzrRYJuQt606u3U94xUjrhXp/hC7/OLKq8js5M3L/F1q6HYhOwt6EThX5rYFLfUstqCXWsU5c9VCK33ogtNmMazRXf1wQKRuF9F1j+ZJ9pKPX2Ug/f6fEqTobSaMckieYJvZB8Gvt2yMQ/6aLqeDWgp4z+PxrUYv6EieTtNNS2jE0/E9lwbF4zRmboM7ycimeOkUrWs45e2ubfdOurltFWrm7cQzSX4XI6ni6M/HuAHdHdVCPPGlzwKCAQBxaZjM5MnL4I9DK8vFchc8nyr6JshVX+g47mdIy/xpwmIC4Ap0H6YXqpSS8O3iFDrq21PWn6mztYivHxIeI1TEG5Df7PHbAiP8dfvc1jP+xFgLRYAKTyD2PZBiCreiwuTu63SatUS5EwXk76MrC588IUu3N80RQxfyvbP2cSdLvDl12mYlTnoMWp1Yb3jIltvbZxRAGLjKEZt4LxHPpl4DbrA7IZ6RETc+e3GrJMs4xjUmJNbJCp3cv0yrlBSCbzHCm3Cne5Q+Bk0PRxVxkmp2qi0IM/yKr68naB0IlO9TzhfCB1WVzS3jdrqmuIJAy2slz3IJxb3qg2je4NzPc6b5AoIBACzQVFMBsYK1Q8h11X1GNNLWNr3pfFEhQdH65LxTqlXcloPQFf1QWYKtCUQ4O9T8XH3GcytgbtTleZBK5zFH2eFIJcj8UiTj6T52XmT8elTLdLLAapEZMclCMrDGQemg/VY0tzLNCCCOfBe0vtW0WH8AkXyJTU3zPDm8lH4XaVlNecyZ11CwoHOW8Y3bLQ05E3UP47zTHwsjCTA11iqp8HnW6soELDQtfRKG3CEOD0R7zMC7Z17P/0eBAyEte1zCj3TYumOD5FhDb8R43hSDxkOsILH+OhdR7SWmYtFp9dandKE0nMl0fVPWqgby3+IN0cCpz0yMjhNUqd8jFfqxUvcCggEAc4p5TmMMiT7o0q/vbfNwNvaFRLZghdJGnA4urBGTUZ9ka1Dn1eyZXISOJdDy2m/BiD9Q7vFjg5VuQ0h3suc0JR+r8gHmWlgF0wtnzN5GapCYoz0D4oJ5TP4un5rcEljS+/mEmEWBtErAsY8mDkIi6D3FgTAYPu5NxlylmsLhAkMqMzALcx+FV++oz1PFhHjUogtbHPro2sZtcLlXt3VmaowXS6X1t5ivk7UDysrTb/jCwWTX5UoFWjxWwgcVGQo5EyElEE24WkmyZgOCX0vDERQ77RKCS85VIXW8wdXoNz8R7DkmnZFDOcq6PWv8rEA2lCA0W6IcVnEL9/CL45Iglg==',
            'LLPAY_PUBLIC_KEY'=>'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCSS/DiwdCf/aZsxxcacDnooGph3d2JOj5GXWi+q3gznZauZjkNP8SKl3J2liP0O6rU/Y/29+IUe+GTMhMOFJuZm1htAtKiu5ekW0GlBMWxf4FPkYlQkPE0FtaoMP3gYfh+OwI+fIRrpW3ySn3mScnc6Z700nU/VYrRkfcSCbSnRwIDAQAB',
            'callbackUrl'=>'',
            'serverCallUrl'=>'',
            'gatewayUrl'=>'http://mertest.unicompayment.com/CashierWeb/servlet/MerOrderPaySharingReqServlte.htm?reqCharSet=UTF-8',
            'refundUrl'=>'http://mertest.unicompayment.com/pay/trade/singleRefund.htm?reqCharSet=UTF-8',
        ];

        $this->payment = new Util([]);
    }


    /**
     * Web支付
     * @param array $data
     * @return mixed|string
     *  $data = [
     *      "order_sn" => "DD201805163652374"  //订单号
     *      "amount" => 0.01       //订单金额
     *      "pay_from" => "order"  //订单来源： order:订单，recharge:充值
     *  ]
     */
    public function payWebHandle(array $data)
    {
        $result = [
            'status'=> false,
            'message'=>'fail',
            'data'=>[],
        ];
        $this->pay_way = 'wopay';
        //获取订单基本信息
        $payInfo = $this->getPayInfo($this->pay_way);
        //构造要请求的参数数组，无需改动
        $parameter = array (
            "interfaceVersion" => $this->config['interfaceVersion'],
            "tranType" => trim($this->config['tranType']),
            "merNo" => trim($this->config['merNo']),
            "goodsName" => $data['goodsName'],
            "orderDate" => date('YmdHis',strtotime($data['created_at'])),
            "orderNo"=> $data['orderNo'],
            "amount"=> $data['amount']*100,
            "charSet" =>trim($this->config['charSet']),
            'reqTime'=> date('YmdHis'),
            'respMode'=>trim($this->config['respMode']),
            'callbackUrl'=> $payInfo['return_url'],
            'serverCallUrl'=> $payInfo['notify_url'],
            "signType"=>trim($this->config['signType']),
        );
        try{
            $sortPara = $this->payment->buildRequestPara($parameter);
            $html = RequestHelper::postForm($this->config['gatewayUrl'],$sortPara);
            $result['status'] = true;
            $result['message'] = 'success';
            $result['data'] =$html;
        }catch (\Exception $e){
            $result['message'] = $e->getMessage();
        }
        return $result;
    }

    public function refundHandle(array $data)
    {
        $result = [
            'status'=> false,
            'message'=>'fail',
            'data'=>[],
        ];
        // TODO: Implement payWapHandle() method.
        $parameter = array (
            "refundReqJournl" => '20120101201010000001',
            "merNo" => trim($this->config['merNo']),
            "orderNo"=> $data['orderNo'],
            "orderDate" => date('Ymd',strtotime($data['created_at'])),
            'payJournl'=>$data['payNo'],
            'merReqTime'=> date('YmdHis'),
            "amount"=> $data['amount'],
            'reason'=>$data['reason'],
            "signType"=>trim($this->config['signType']),
        );
        try{
            $sortPara = $this->payment->buildRequestPara($parameter);
            $return  = RequestHelper::postCurl($this->config['gatewayUrl'],$sortPara);
            $sign = $return['signMsg'];
            $status = $this->payment->buildRsaverify($return,$sign);
            if ($return){
                $result['status'] = true;
                $result['message'] = 'success';
                $result['data'] = $status;
            }else{
                $result['message'] = '验签失败';
            }
        }catch (\Exception $e){
            $result['message'] = $e->getMessage();
        }
        return $result;
    }

    public function payAppHandle(array $data)
    {
        // TODO: Implement payWapHandle() method.
    }



    /**
     * 回调处理
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function notifyHandle(array $data)
    {
        $sign = $data['signMsg'];
        unset($data['signMsg']);
        $status = $this->payment->buildRsaverify($data,$sign);
        if($status){
            if(isset($data['transRst']) && $data['transRst'] == 1){
                return $this->orderNotifyHandle($data);
            }else{
                throw new \Exception('支付失败，订单号：'.$data['orderNo']);
            }
        }else{
            throw new \Exception('校验失败，订单号：'.$data['orderNo']);
        }
    }

    /**
     * 订单支付回调数据处理
     * @param $data
     * @return array
     */
    private function orderNotifyHandle($data){
        $orderPayData = array();
        $orderPayData['status'] = 1;//已收款
        $orderPayData['code'] = $data['oid_paybill'];//第三方流水号
        $orderPayData['completed_at'] = date('Y-m-d H:i:s');
        $orderPayData['orderId'] = $data['orderNo'];//商户订单号
        $orderPayData['amount'] = $data['amount']/100;
        $orderPayData['orderDate'] = $data['orderDate'];
        $orderPayData['payJnlno'] = $data['payJnlno'];
        $orderPayData['payTime'] = $data['payTime'];
        return $orderPayData;
    }



}